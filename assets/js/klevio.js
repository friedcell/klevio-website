// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
	Object.keys = (function keys() {
		'use strict';
		var hasOwnProperty = Object.prototype.hasOwnProperty,
			hasDontEnumBug = !({ toString: null }).propertyIsEnumerable('toString'),
			dontEnums = [
				'toString',
				'toLocaleString',
				'valueOf',
				'hasOwnProperty',
				'isPrototypeOf',
				'propertyIsEnumerable',
				'constructor'
			],
			dontEnumsLength = dontEnums.length;
		return function (obj) {
			if (typeof obj !== 'function' && (typeof obj !== 'object' || obj === null)) {
				throw new TypeError('Object.keys called on non-object');
			}
			var result = [], prop, i;
			for (prop in obj) {
				if (hasOwnProperty.call(obj, prop)) {
					result.push(prop);
				}
			}
			if (hasDontEnumBug) {
				for (i = 0; i < dontEnumsLength; i++) {
					if (hasOwnProperty.call(obj, dontEnums[i])) {
						result.push(dontEnums[i]);
					}
				}
			}
			return result;
		};
	}());
}
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(search, this_len) {
		if (this_len === undefined || this_len > this.length) {
			this_len = this.length;
		}
		return this.substring(this_len - search.length, this_len) === search;
	};
}

// KlevioData
(function () {
	document.body.classList.add("js-on");
	if (!window.Klevio) {
		window.Klevio = {};
	}
	window.Klevio = Object.assign(window.Klevio, {
		GTM: "GTM-NGZNJG8",
		settings: (function Settings() {
			let loadedValues = null;
			const 
				p = Object.freeze({
					// objects need to be simple as freeze only freezes first level; if you want to nest, you have to run freeze on subobjects manually 
					"currentValues": Object.freeze({
						"essential": true,
						"analytics": "20190318"
					}),
					"defaultValues": Object.freeze({
						"essential": false,
						"analytics": false
					}),
					"GTM": Object.freeze({
						"20190318": "GTM-MRVFF5W",
						true: "GTM-T8QLB5",
						false: "GTM-NGZNJG8"
					}),
					"_value": function _value() {
						let settings = localStorage.getItem("klevioPrivacy");
						return settings ? JSON.parse(settings) : null;
					},
					"load": function load() {
						let settings = p._value();
						if (settings === null) {
							loadedValues = p.defaultValues;
						} else {
							// add defaults if not present in object
							Object.keys(p.defaultValues).forEach(function (key) {
								if (!(key in settings)) {
									settings[key] = p.defaultValues[key];
								}
							});
							loadedValues = Object.freeze(settings);
						}
						return loadedValues;
					},
					"save": function save(settings) {
						// don't lose values that are currently loaded and are not default values
						Object.keys(loadedValues).forEach(function (key) {
							if (!(key in settings) && loadedValues[key] !== p.defaultValues[key]) {
								settings[key] = loadedValues[key];
							}
						});
						localStorage.setItem("klevioPrivacy", JSON.stringify(settings));
						p.load();
					}
				}),
				r = Object.freeze({
					"load": function load(force) {
						if (force || loadedValues === null) {
							p.load();
						}
						return loadedValues;
					},
					"GTM": function GTM() {
						return p.GTM[loadedValues.analytics];
					},
					"saveChoice": function saveChoice(choice) {
						let settings = p._value();
						if (!settings) {
							settings = {};
						}
						// don't lose non-default properties
						settings["essential"] = (!!choice) && p.currentValues.essential;  // save false or current values
						settings["analytics"] = (!!choice) && p.currentValues.analytics;
						p.save(settings);
						return loadedValues;
					},
					"saveProperty": function saveProperties(data) {
						let settings = p._value(),
							needsSave = false;
						if (settings === null) {
							settings = {};
						}
						Object.keys(data).forEach(function (key) {
							if (!(key in p.defaultValues)) {
								settings[key] = data[key];
								needsSave = true;
							}
						});
						if (needsSave) {
							p.save(settings);
						}
						return loadedValues;
					},
					"needUserAction": function needUserAction() {
						let settings = p._value(),
							r = false;
						if (settings === null || !("modified" in settings)) {  // either not present or old style object without modify
							return true;
						} else if (settings["modified"] && (new Date()).getTime() - settings["modified"] > 86400 * 1000 * 7) {  // only annoy people once a week
							// in case we have a new currentValue - we want users to reconfirm, so we can give them newer tag
							Object.keys(settings).forEach(function (key) {
								if (key in p.defaultValues && settings[key] !== p.defaultValues[key] && settings[key] !== p.currentValues[key]) {
									r = true;
								}
							});
							if (r) {return r;} // fast exit
							// in case user clicked x and essential/analytics is not current
							if (settings["source"] && settings["source"].endsWith(".notice__close") && 
								(settings["essential"] !== p.currentValues["essential"] || settings["analytics"] !== p.currentValues["analytics"])) {
								return true;
							}
						}
						return r;
					},
					"needUserUpdate": function needUserUpdate() {
						let r = false;
						Object.keys(loadedValues).forEach(function (key) {
							if (key in p.currentValues && loadedValues[key] !== p.currentValues[key]) {
								r = true;
							}
						});
						return r;
					},
				});
			return r;
		})()
	});
}());
// /KlevioData

// PRIVACY
(function () {
	function noticeVisible(visible) {
		let notice = document.querySelector(".notice");
		if (visible) {
			notice.classList.add('on');
		} else {
			notice.classList.remove('on');
		}
	}
	function loadAnalytics() {
		let script = document.createElement("script");
		script.src = "/assets/js/analytics.js";
		document.body.appendChild(script);
	}
	function loadIntercom() {
		// Intercom
		let APP_ID = 'rmujacxa';
		let showIntercom = document.location.hash === '#show-intercom';
		// TODO - if buy page, add vertical_padding: 100px or whatever to intercomSettings to adapt for bottom banner
		window.intercomSettings = {
			app_id: APP_ID,
			hide_default_launcher: window.location.pathname.search('/installation') === 0,
			custom_launcher_selector: '.intercom-launcher',
		};
		if (document && document.body) {
			document.body.classList.add('intercom');
			/*
			const w = (document.documentElement? document.documentElement.clientWidth : 0) || window.innerWidth || document.body.clientWidth;
			const isSmall = w <= 760;
			*/
			let win = window;
			let ic = win.Intercom;
			if (typeof ic === "function") {
				ic('reattach_activator');
				ic('update', intercomSettings);
			} else {
				// Intercom object call queue
				let i = function () {
					i.c(arguments);
				};
				i.q = [];
				i.c = function(args) {
					i.q.push(args);
				};
				win.Intercom = i;
				// load script
				let s = document.createElement('script');
				s.async = true;
				s.onload = function () {
					if (showIntercom) {
						window.Intercom('show');
					}
				};
				s.src = 'https://widget.intercom.io/widget/' + APP_ID;
				document.body.appendChild(s);
			}
		}
	}
	function processSettings(settings) {
		noticeVisible(false);
		if (settings.essential) {
			loadIntercom();
		}
		window.Klevio.GTM = window.Klevio.settings.GTM();
	}
	function saveSettings(value) {
		const previousSettings = window.Klevio.settings.load();
		let settings;
		// save if we have an explicit value or previous settings are false
		if (value != null || (previousSettings.essential === false && previousSettings.analytics === false)) {
			settings = window.Klevio.settings.saveChoice(!!value);
		}
		settings = window.Klevio.settings.saveProperty({
			"modified": (new Date()).getTime()
		});
		// essential is enabled, but wasn't before - reload
		if (settings.essential && !previousSettings.essential) {
			document.location.reload();
		} else {
			processSettings(settings);
		}
	}
	function detectNoTracking() {
		var $forms = document.querySelectorAll("#mc_embed_signup form"),
			$mc_check = document.querySelectorAll("#mc_embed_signup .mc-field-group"),
			mc_works = true,
			currentStyle;
		for (i = 0, j = $forms.length; i < j; i += 1) {
			$forms[i].addEventListener("submit", function (ev) {
				if (mc_works && $mc_check.length) { // only recheck if it looks like it works
					currentStyle = window.getComputedStyle($mc_check[0]);
					if (currentStyle && currentStyle["clear"] === "none" && currentStyle["min-height"] === "0px") {
						mc_works = false;
					}
				}
				if (!mc_works) {
					let $response = this.querySelector("#mce-error-response");
					if ($response) {
						$response.innerHTML = "Form doesn't work. Do you have Tracking protection enabled?";
						$response.style.display = 'block';
					}
				}
			});
		}
	}
	function init() {
		const $confirmBtn = document.querySelector("#notice-confirm"),
			$statusConfirmed = document.querySelector("#notice__status--accepted"),
			$statusDenied = document.querySelector("#notice__status--denied"),
			toggleCurrentStatus = $statusConfirmed && $statusDenied && true;
		let confirmed = false;
		
		if (!$confirmBtn) {
			document.addEventListener("DOMContentLoaded", init, false);
			return;
		}
		$confirmBtn.addEventListener('click', function () {
			saveSettings(true);
			if (toggleCurrentStatus) {
				document.location.reload();
			}
		}, false);
		function getTargetIdent(elm, tagName) {
			tagName = tagName.toLowerCase();
			let ident = "";
			while (elm.tagName.toLowerCase() !== tagName && elm.parentNode) {
				elm = elm.parentNode;
			}
			if (elm.tagName.toLowerCase() === tagName) {
				if (elm.id) {
					ident += "#" + elm.id;
				}
				if (elm.className) {
					ident += "." + elm.className.split(" ").join(".");
				}
			}
			return ident;
		}
		// every page except privacy policy - pressing x keeps current settings
		let $closeBtn = document.querySelector(".notice__close");
		if ($closeBtn) { 
			$closeBtn.addEventListener('click', function (ev) {
				saveSettings(null);
				let ident = getTargetIdent(ev.target, "button");
				window.Klevio.settings.saveProperty({"source": window.location.pathname + ident});
			}, false);
		}
		// privacy policy page
		let $denyBtn = document.querySelector("#notice-close");
		if ($denyBtn) {
			$denyBtn.addEventListener('click', function (ev) {
				saveSettings(false);
				let ident = getTargetIdent(ev.target, "button");
				window.Klevio.settings.saveProperty({"source": window.location.pathname + ident});
				document.location.reload(); // just reload...
			}, false);
		}
		const settings = window.Klevio.settings.load();
		if (window.Klevio.settings.needUserAction()) {
			if (settings.analytics !== false) {  // change text to indicate update happened
				document.querySelectorAll(".notice__description").forEach(function (elm) {
					elm.innerHTML = 'We have updated the list of third party tools we use to improve your experience. Some of them\n' +
						'are necessary to enable you to chat to us, others are used to improve \n' +
						'the performance of the website and for tailored advertising. For more \n' +
						'information, visit our\n' +
						'<a href="/legal/privacy-policy.html">Privacy Policy</a>';
				});
			}
			noticeVisible(true);
		} else {
			processSettings(settings);
		}
		if (toggleCurrentStatus) {
			if (window.Klevio.settings.needUserUpdate()) {
				$statusConfirmed.innerHTML = "You have <strong>accepted</strong> our <strong>previous</strong> privacy policy."
			}
			if (settings.essential && settings.analytics) {
				$statusDenied.classList.add("notice__status--hide");
				$statusConfirmed.classList.remove("notice__status--hide");
			} else {
				$statusDenied.classList.remove("notice__status--hide");
				$statusConfirmed.classList.add("notice__status--hide");
			}
		}
		// we always load analytics - tag manager handles what we're loading based on GTM version
		loadAnalytics();
		detectNoTracking();
	}

	init();
}());

(function () {
	let skipSelect = false;

	function closeIntercomOverlay() {
		let intercom = document.querySelector(".contact");
		intercom.classList.add('contact--hide');
	}

	function attachShowIntercomHandlers(intercom) {
		let closeEls = [intercom, intercom.querySelector('.contact__close')];
		closeEls.forEach(function (el) {
			el.addEventListener('click', function (e) {
				intercom.classList.add('contact--hide');
				e.stopPropagation(); e.preventDefault();
			}, false);
		});

		intercom.querySelector('.contact__wrap').addEventListener('click', function (e) {
			e.stopPropagation();
		}, false);

		intercom.querySelector('#contact-confirm').addEventListener('click', handleSelection, false);
	}

	function handleSelection(e) {
		const method = document.querySelector('[name="contact-comm"]:checked').value;

		closeIntercomOverlay();
		if (method === 'chat') {
			const settings = {
				"essential": true,
				"analytics": false
			};
			localStorage.setItem("klevioPrivacy", JSON.stringify(settings));
			const url = document.location.pathname + '#show-intercom';
			document.location.replace(url);
			document.location.reload();  // Previous line just changes URL
		} else {
			skipSelect = true;
			document.querySelector('.intercom-launcher').click();
		}
	}

	function showIntercomWarning(e) {
		if (!skipSelect) {
			let intercom = document.querySelector(".contact");
			intercom.classList.remove('contact--hide');
			attachShowIntercomHandlers(intercom);
			e.preventDefault(); e.stopPropagation();
		}
	}

	function init() {
		let settings = JSON.parse(localStorage.getItem("klevioPrivacy"));
		if (!settings || !settings.essential) {
			const intercoms = Array.from(
					document.querySelectorAll('.intercom-launcher')
			);
			intercoms.forEach(function (launcher) {
				launcher.addEventListener('click', showIntercomWarning, false);
			});
		}
	}

	init();
}());
// PRIVACY

// NEWSLETTER OVERLAY
(function () {
	const popupDelay = 0; // Show pop-up after popupDelay seconds - if 0 it is disabled
	let overlay = document.querySelector('.newsletter');
	function cloneEmailForm() {
		const origHTML = document.querySelector('.footer__items__item__form').innerHTML.trim();
		let wrap = document.createElement('div');
		wrap.innerHTML = origHTML;
		let els = Array.from(wrap.querySelectorAll('[id]'));
		els.forEach(function (el) {
			el.removeAttribute('id');
		});
		wrap.firstChild.id = 'mc_embed_signup-2';
		return wrap.removeChild(wrap.firstChild);
	}
	function swapEmailForms() {
		let form1 = document.querySelector("#mc_embed_signup");
		let dad1 = form1.parentNode;
		let form2 = document.querySelector("#mc_embed_signup-2");
		let dad2 = form2.parentNode;
		resetForm(); // Reset any old state first
		dad1.removeChild(form1);
		dad2.removeChild(form2);
		dad1.appendChild(form2);
		dad2.appendChild(form1);
	}
	function resetForm() {
		let els = Array.from(
			document.querySelectorAll('#mce-success-response, #mce-error-response')
		);
		els.forEach(function (el) {
			el.innerHTML = '';
			el.style.display = 'none';
		});
	}
	function dontShowAgain() {
		window.Klevio.settings.saveProperty({"hideNewsletterOverlay": (new Date().getTime())});
	}
	function showNewsOverlay() {
		swapEmailForms();
		overlay.classList.remove("overlay--hide");
	}
	function displayOverlay() {
		const settings = window.Klevio.settings.load();
		// used to use bool, convert to time - we haven't used this in 2020, so should be fine if we use 2020-01-01
		if (settings.hideNewsletterOverlay === true) {
			window.Klevio.settings.saveProperty({"hideNewsletterOverlay": (new Date(2020,0,1).getTime())})
		}
		return !settings.hideNewsletterOverlay;
	}
	function closeOverlay(e) {
		overlay.classList.add('overlay--hide');
		swapEmailForms();
		dontShowAgain();
		e.stopPropagation(); e.preventDefault();
	}
	function init() {
		let overlay_content = document.querySelector('.newsletter__content');
		if (overlay_content) {
			overlay_content.addEventListener('click', function (e) {
				e.stopPropagation(); 
			}, false);
			overlay.addEventListener('click', closeOverlay, false);
			document.querySelector(".newsletter__close").addEventListener('click', closeOverlay, false);
		}
		let formWrap = document.querySelector('.newsletter-form');
		if (formWrap) {
			let formCopy = cloneEmailForm();
			formWrap.appendChild(formCopy);
		}
	}
	console.log(displayOverlay(), overlay, popupDelay);
	if (displayOverlay() && overlay && popupDelay) {
		init();
		setTimeout(showNewsOverlay, popupDelay * 1000);
	}
}());
// /NEWSLETTER

// OFFER OVERLAY
(function () {
	function init() {
		let offer_cta = document.querySelector('.offer__cta');
		let overlay = document.querySelector('.overlay--offer');
		if (offer_cta && overlay) {
			let overlay_close = overlay.querySelector('.overlay__close');
			if (overlay_close) {
				overlay_close.addEventListener('click', function (e) {
					overlay.classList.add("overlay--hide");
					e.stopPropagation();
					e.preventDefault();
				});
				offer_cta.addEventListener('click', function (e) {
					overlay.classList.remove("overlay--hide");
					e.stopPropagation();
					e.preventDefault();
				});
			} else {
				console.log("Overlay close not found:", overlay_close);
			}
		} else {
			if (offer_cta || overlay) {
				console.log("Only one of two required elements found:", offer_cta, overlay);
			}
		}
	}
	init();
}());
// /OFFER OVERLAY

// EVENTS
(function () {
	if (document.querySelectorAll && document.querySelector) {
		let $events = document.querySelectorAll(".events__details");
		let $container = document.querySelector(".events");
		let $title = document.querySelector(".events__title");
		let now = new Date();
		if ($events.length && $title && $container && $container.classList && $container.dataset) {
			let hasEvents = false;
			let i = 0, j = $events.length;
			for (; i < j; i += 1) {
				let $details = $events[i];
				let $dtstart = $details.querySelector(".dtstart");
				let $dtend = $details.querySelector(".dtend");
				if ($dtend && $dtstart) {
					let showDate = new Date($details.dataset["showfrom"]);
					let startDate = new Date($dtstart.title);
					let endDate = new Date($dtend.title);
					endDate.setDate(endDate.getDate() + 1); // add one day so we show it on last day of event
					// show event if between
					if (now > showDate && now < endDate && !hasEvents) { // only ever show one event - first we find that matches
						// change title for currently ongoing event
						if (now > startDate) {
							$title.innerHTML = $details.dataset["headline"] || "Current event";
						}
						$details.classList.remove("hidden");
						hasEvents = true;
					} else {
						$details.classList.add("hidden");
					}
				}
			}
			// if no valid events found, hide the whole block
			if (!hasEvents) {
				$container.classList.add("hidden");
			}
		}
	}
}());
// /EVENTS

// SWIPE
(function() {
	//swipedetect function credit: http://www.javascriptkit.com/javatutors/touchevents2.shtml
	function swipedetect(el, callback) {
		var touchsurface = el,
			swipedir,
			startX,
			startY,
			distX,
			distY,
			threshold = 30, //required min distance traveled to be considered swipe
			restraint = 200, // maximum distance allowed at the same time in perpendicular direction
			allowedTime = 300, // maximum time allowed to travel that distance
			elapsedTime,
			startTime,
			handleswipe = callback || function (swipedir) {};
		touchsurface.addEventListener('touchstart', function (e) {
			var touchobj = e.changedTouches[0];
			swipedir = 'none';
			distX = 0;
			distY = 0;
			startX = touchobj.pageX;
			startY = touchobj.pageY;
			startTime = new Date().getTime();  // record time when finger first makes contact with surface
		}, false);
		touchsurface.addEventListener('touchend', function (e) {
			var touchobj = e.changedTouches[0];
			distX = touchobj.pageX - startX;  // get horizontal dist traveled by finger while in contact with surface
			distY = touchobj.pageY - startY;  // get vertical dist traveled by finger while in contact with surface
			elapsedTime = new Date().getTime() - startTime;  // get time elapsed
			if (elapsedTime <= allowedTime) {  // first condition for awipe met
				if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) {  // 2nd condition for horizontal swipe met
					swipedir = (distX < 0)? 'left' : 'right';  // if dist traveled is negative, it indicates left swipe
				}
				else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
					swipedir = (distY < 0)? 'up' : 'down';  // if dist traveled is negative, it indicates up swipe
				}
			}
			handleswipe(swipedir);
		}, false);
	}
	function getNextRadio(component, offset) {
		const radios = component.querySelectorAll('[type="radio"]');
		const selected = component.querySelector('[type="radio"]:checked');
		const sel_index = selected ? parseInt(selected.value, 10) - 1 : 0;
		const new_index = (radios.length + sel_index + offset) % radios.length;
		return radios[new_index];
	}
	function createHandler(component, offset) {
		return function (e) {
			const radio = getNextRadio(component, offset);
			radio.click();
			e.preventDefault();
			e.stopPropagation();
		};
	}
	function createArrow(direction) {
		const arrow = direction === 'left' ? 'previous' : 'next';
		let node = document.createElement('div');
		node.classList.add('nav__control', direction, "nav__control--" + direction);
		node.innerHTML = '<svg class=""><use xlink:href="#icon-' + arrow + '" /></svg>';
		return node;
	}
	function createControl(component, direction) {
		const node = createArrow(direction);
		const offset = direction === 'left' ? -1 : 1;
		node.addEventListener('click', createHandler(component, offset), false);
		return node;
	}
	function initComponent(component, controls_container, autoplay, autoplay_controls) {
		if (!component.classList.contains('js-init')) {
			component.classList.add('js-init');
			let strip = component.querySelector(controls_container);
			if (!strip) {
				return;
			}
			strip.appendChild(createControl(component, 'left'));
			strip.appendChild(createControl(component, 'right'));
			swipedetect(component, function (swipedir){
				if (swipedir === 'left'){
					const radio = getNextRadio(component, 1);
					radio.click();
					component.classList.add('js-go-manual');
				}
				else if (swipedir === 'right') {
					const radio = getNextRadio(component, -1);
					radio.click();
					component.classList.add('js-go-manual');
				}
			});
			if (autoplay) {
				// Clicking on controls turns off automatic switch
				const controls = component.querySelectorAll(autoplay_controls);
				for (let i=0, j=controls.length; i < j; i++) {
					controls[i].addEventListener('click', () => {
						component.classList.add('js-go-manual');
					}, false);
				}
				// Select first option and start animation
				const radios = component.querySelectorAll('[type="radio"]');
				if (radios.length) {
					radios[0].click();
				}
				const intv = setInterval(() => {
					if (!component.classList.contains('js-go-manual')) {
						const radio = getNextRadio(component, 1);
						radio.click();
					} else {
						clearInterval(intv);
					}
				}, 15 * 1000);
			}
		}
	}
	function init(component_selector, controls_container, autoplay) {
		const components = document.querySelectorAll(component_selector + ":not(.js-init)");
		for (let i=0, j=components.length; i<j; i++) {
			initComponent(components[i], controls_container, !!autoplay);  // !! normalized undefined
		}
	}
	init('.list', '.list__items-wrap', false);
	init('.testimonial', '.carousel', true);
}());
