// ANALYTICS

// Load Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer', window.Klevio.GTM);

(function () {
	let gaID = 'UA-82041259-1';
	let intersectionThreshold = 0.5;
	/*
	window.dataLayer = window.dataLayer || [];
	window.gtag = function () {
		dataLayer.push(arguments);
	};
	gtag('js', new Date());
	gtag('config', gaID);
	*/
	
	function createTracker() {
		if (window.ga) {
			if (!window.ga.initialised) {
				window.ga('create', gaID, 'auto', {"name": "klevio"});
				window.ga('klevio.require', 'ecommerce');
				window.ga.initialised = true;
			}
		}
	}

	function sendEvent(category, action, label, value) {
		if (window.ga) {
			createTracker();
			window.ga('klevio.send', 'event', category, action, label, value);
		}
		if (window.fbq) {
			window.fbq('trackCustom', label, value);
		}
	}
	if (window.sendEvent) {
		let i = 0, j = (window.sendEvent._list || []).length;
		for (; i < j; i += 1) {
			sendEvent.apply(null, window.sendEvent._list[i]);
		}
	}
	window.sendEvent = sendEvent;

	function createEventHandler(category, action, label, value) {
		return function () {
			createTracker();
			sendEvent(category, action, label, value);
		};
	}

	function attachEvent(selector, event_name, handler) {
			const controls = document.querySelectorAll(selector);
			for(let i=0; i < controls.length; i++) {
				controls[i].addEventListener(event_name, handler, false);
			}
	}

	function createIntersectionHandler(category, action, label, value) {
		return function (entries) {
			entries.forEach(function (entry) {
				if (entry.intersectionRatio > intersectionThreshold) {
					createTracker();
					sendEvent(category, action, label, value);
				}
			});
		};
	}

	function observe(selector, handler) {
		const options = {
			root: null,
			rootMargin: "0px",
			threshold: intersectionThreshold
		};

		if (window.IntersectionObserver) {
			const control = document.querySelector(selector);
			if (control) {
				let observer = new IntersectionObserver(handler, options);
				observer.observe(control);
			}
		}
	}

	createTracker();

	// Newsletter sign up
	attachEvent('#mc-embedded-subscribe', 'click', createEventHandler('engagement', 'Signup', 'signup_nl'));
	// Contact us
	attachEvent('a[href^="mailto:sales@klevio"]', 'click', createEventHandler('engagement', 'Click', 'email_b2b'));
	attachEvent('a[href^="mailto:hello@klevio"]', 'click', createEventHandler('engagement', 'Click', 'email_b2c'));
	// Read blog
	attachEvent('a[href^="https://medium.com/@weareklevio"]', 'click', createEventHandler('engagement', 'Click', 'read_blog'));
	// App download
	attachEvent('a[href^="https://itunes.apple.com"]', 'click', createEventHandler('engagement', 'Click', 'dl_ios'));
	attachEvent('a[href^="https://play.google.com"]', 'click', createEventHandler('engagement', 'Click', 'dl_android'));
	// Brochure
	attachEvent('.btn--brochure', 'click', createEventHandler('engagement', 'Download', 'dl_b2b_brochure'));
	attachEvent('.brochure-shortlet-hosts', 'click', createEventHandler('engagement', 'Download', 'dl_slh_brochure'));
	attachEvent('.brochure-shortlet-managers', 'click', createEventHandler('engagement', 'Download', 'dl_slm_brochure'));
	// Airbnb
	attachEvent('.analytics__airbnb--newlisting', 'click', createEventHandler('engagement', 'Click', 'start_listing'));
	attachEvent('.analytics__airbnb--newlistingemail', 'click', createEventHandler('engagement', 'Click', 'new_listing_email'));
	attachEvent('.analytics__airbnb--existinglistingemail', 'click', createEventHandler('engagement', 'Click', 'existing_listing_email'));
	attachEvent('.analytics__airbnb--superhostemail', 'click', createEventHandler('engagement', 'Click', 'superhost_email'));
	attachEvent('.analytics__airbnb--hosting', 'click', createEventHandler('engagement', 'Click', 'findout_hosting'));
	// Scroll events
	observe('.cart__title', createIntersectionHandler('scroll', 'scroll', 'scroll_package'));
	observe('.total__title', createIntersectionHandler('scroll', 'scroll', 'scroll_total'));
}());

// FB pixel
(function(){
	var form = document.getElementById("mc-embedded-subscribe-form");
	if (form) {
		form.addEventListener("submit", function () {
			for (var i=0, j=form.length; i<j; i+=1) {
				if (window.fbq && form[i].name === "group[7105][1]" && form[i].checked) {
					fbq('track','Lead', {content_name:"Newsletter"});
				} else if (window.fbq && form[i].name === "group[7105][2]" && form[i].checked) {
					fbq('track','InitiateCheckout', {content_name:"Beta"});
				}
			}
		});
	}
})();
// END OF ANALYTICS
